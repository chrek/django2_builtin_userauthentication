# django2_builtin_userauthentication
A simple website written in Django with login and logout authentication system.

----

This web site is using the Django built-in login and logout system that is provided by its 
Core authentication framework (auth app) and its default models.

The main features that have currently been implemented are:

* Authentication pages are provided at the project level
* The login can be shared across multiple applications because it's available across the whole site
* The auth app provides us with several authentication views and URLs for handling login, logout, and password management.
* Users accessing the home page are asked to use the provided login link in order to log include
* A successful login presents a welcome page with a logout link

Start the Django dev server with `py manage.py runserver` 

## Access the website
Access the website from the browser using `http://localhost:8000/`

## References

1.	[wsvincent](https://wsvincent.com/django-user-authentication-tutorial-login-and-logout/)
2.	[groups.google](https://groups.google.com/forum/#!topic/django-users/BWFBOrDb3-c)
3.	[developer.mozilla](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Authentication)


